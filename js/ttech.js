
/*  custom functions
*   author: Ren+Rey
*/

jQuery(document).ready(function() {
    // change reviews to projects
    jQuery('.carousel.carousel-reviews > .carousel-title span').html('projects');

    // remove side bar on features page (service)
    jQuery('body.single-benefit #main > .container > .row > .col-xs-12.col-sm-12.col-md-3').hide();
    jQuery('body.single-benefit #main > .container > .row > .col-xs-12.col-sm-12.col-md-9').removeClass('col-md-9');
    
    // add fancybox functionality here
    jQuery('.enlargeable').click(function() {
        
    });

    if (jQuery('.ttech-custom-content').length > 0) { // this is the t-tech custom page
        // insert underline after each h2
        jQuery('<div class="line transform-please-2"></div>').insertAfter(jQuery('.ttech-custom-content > div > h2'));

        // removes make href and target attributes
        jQuery('.ttech-custom-nav > a')
            .removeAttr('href')
            .removeAttr('target');
        
        var all_expandos = jQuery('.expandos > .expando'),
            model_index,
            make_index,
            all_makes = jQuery('.ttech-custom-content > div'),
            this_object,
            this_expando,
            keys = [],
            curr_key, 
            this_brand, 
            brand_name;
        
        function resetTTechCustom() {
            if (jQuery('.ttech-custom-intro').is(':visible')) {
                jQuery('.ttech-custom-intro').slideUp();
            }
            all_expandos.hide();
            jQuery('ul.models li').removeClass('selected');            
        }
        
        function selectExpandos(the_key) {
            all_expandos.each(function() {
                this_expando = jQuery(this);
                keys = jQuery.parseJSON('[' + this_expando.data("keys") + ']');
                keys.toLowerCase();
                if (jQuery.inArray(the_key, keys) > -1) {
                    this_expando.slideDown(600, 'linear');
                }
            });
        }
        
        //functionality of the brands selector
        jQuery('.brands ul li').click(function() {
            all_makes.hide();
            this_brand = jQuery(this);
            brand_name = this_brand.children('span').text();
            brand_name = brand_name.toLowerCase();
            resetTTechCustom();
            selectExpandos(brand_name);
        });

        // functionality of the model selector
        jQuery('ul.models li').click(function() {
            jQuery('ul.models li').removeClass('selected');
            this_object = jQuery(this);
            curr_key = this_object.children('h3').text();
            curr_key = curr_key.toLowerCase();
            resetTTechCustom();
            this_object.addClass('selected');
            selectExpandos(curr_key);
        });

        // functionality of the make selector
        jQuery('.ttech-custom-nav > a').click(function() {            
            make_index = jQuery(this).index() + 1;
            all_makes.hide();
            resetTTechCustom();
            jQuery('.ttech-custom-content > div:nth-child(' + make_index + ')').slideDown(600, 'linear');
            return false;
        });
    }
});

/* extend array functionality */
Array.prototype.toLowerCase = function() {
    for (var i = 0; i < this.length; i++) {
        this[i] = this[i].toString().toLowerCase();
    }
}