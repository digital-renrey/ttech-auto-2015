<?php

/* -------------------------------------------------------
 You can add your custom functions below
-------------------------------------------------------- */

add_action('wp_enqueue_scripts', 'load_javascript_files');
function load_javascript_files() {
    
    //wp_register_script('ttech_js', get_template_directory_uri() . '../ttech/js/ttech.js', array('jquery'), true );
    wp_register_script('ttech_js', 'http://ttechauto.lightningbasehosted.com/wp-content/themes/ttech/js/ttech.js', array('jquery'), true );
    wp_enqueue_script('ttech_js');
    
    // fancybox js files
    //wp_register_script('fancybox_js', 'http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js', array('jquery'), true );
    //wp_enqueue_script('fancybox_js');
}